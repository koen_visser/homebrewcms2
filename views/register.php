<?php
/**
 * Register.php - renders a user registration form
 * 
 * @author Bugslayer
 * 
 */
?>
<div class="panel panel-default">
	<div class="panel-heading">
		<h1>Registreer Nieuwe Gebruiker</h1>
		<p>Vul dit formulier in om een nieuwe gebuiker in te kunnen laten
			loggen op de @@project site</p>
	</div>
	<div class="panel-body">
		<form class="form-horizontal" name="input"
			action="?action=save&page=register" method="post">
			<input type="hidden" name="send" value="true" />
			<div class="form-group">
				<label class="control-label col-sm-2" for="Userid">Gebruikersnaam *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Userid" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Password">Wachtwoord *</label>
				<div class="col-sm-10">
					<input class="form-control" type="password" name="Password" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="PwdRetype">Herhaal wachtwoord *</label>
				<div class="col-sm-10">
					<input class="form-control" type="password" name="PwdRetype" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Name_first">Voornaam *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Name_first" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Name_middle">Tussenvoegsel</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Name_middle" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Name_last">Achternaam *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Name_last" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="email">Email Adres *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="email" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Telephone">Telefoonnummer</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Telephone" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Userid">Gebruikersnaam *</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Userid" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Address_street">Adres straat</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Address_street" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Address_number">Huisnummer</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Address_number" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Address_zipcode">Adres postcode</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Address_zipcode" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<label class="control-label col-sm-2" for="Address_city">Adres plaatsnaam</label>
				<div class="col-sm-10">
					<input class="form-control" type="text" name="Address_city" maxlength="50"
						size="30">
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-default">Opslaan</button>
				</div>
			</div>
		</form>
	</div>
</div>
