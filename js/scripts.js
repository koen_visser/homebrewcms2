/**
 * Scripts.js - Some javascript for the navigation menu.
 * 
 * @author Bugslayer
 * 
 */

$(document).ready(function() {
	$('.navbar-top > li').bind('mouseover', function() {
		setSubMenuVisible($(this), true);
	});
	$('.navbar-top > li').bind('mouseout', function() {
		setSubMenuVisible($(this), false);
	});

});

/**
 * Changes the visibility of the
 * <ul>
 * elements in the given element
 * 
 * @param element
 *            the element in which all
 *            <ul>
 *            elements must be changed
 * @param setToVisible
 *            if set to true, the visibility setting is set to visible.
 *            Otherwise it will be set to hidden.
 */
function setSubMenuVisible(element, setToVisible) {
	var newSetting = 'hidden';
	if (setToVisible)
		newSetting = 'visible';
	element.find('ul').css('visibility', newSetting);
};

